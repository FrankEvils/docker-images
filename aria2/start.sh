docker run --restart=always -d --name aria2-ui \
-p 6800:6800 -p 6880:80 -p 6888:8080 \
-v ~/Downloads:/aria2/downloads \
-v $PWD/conf:/aria2/conf \
-e SECRET=frankevil frankevil/aria2-ui
