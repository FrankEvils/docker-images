# docker-images

#### 介绍
docker 常用的镜像配置，docker-compose 配置

#### docker 安装

> 参考官方文档  [https://docs.docker.com/engine/install/centos/](https://docs.docker.com/engine/install/centos/)

```(shell)
curl -fsSL https://get.docker.com -O get-docker.sh && sudo sh get-docker.sh
```
### docker-compose 安装

> 参考官方文档  [https://docs.docker.com/compose/install/standalone/](https://docs.docker.com/compose/install/standalone/)

```（shell）
curl -SL https://github.com/docker/compose/releases/download/v2.19.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose && chmod  +x docker-compose && sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```
### docker 开启远程访问
```(shell)
 curl -fsS https://gitee.com/FrankEvils/docker-images/raw/master/docker%E8%BF%9C%E7%A8%8B%E8%AE%BF%E9%97%AE/generate_certs.sh  -O generate_certs.sh && sh generate_certs.sh
```

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)